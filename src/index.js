const puppeteer = require('puppeteer');
const fs = require('fs-extra');

class D3Scraper {
  urlRoot = 'https://us.diablo3.com';

  typeParam = '#type=';

  pageParam = '&page=';

  constructor(
    page,
    browser,
    itemSlotList = D3Scraper.defaultItemSlotList,
    itemTypeList = D3Scraper.defaultItemTypeList,
  ) {
    this.page = page;
    this.browser = browser;
    this.itemSlotList = itemSlotList;
    this.itemSlotCount = itemSlotList.length;
    this.itemTypeList = itemTypeList;
    this.itemTypeCount = itemTypeList.length;
  }

  escapeXpathString = (str) => {
    const splitedQuotes = str.replace(/'/g, `', "'", '`);
    return `concat('${splitedQuotes}', '')`;
  };

  async scrapeItem(puppeteerElement, classSpecific) {
    const item = await puppeteerElement.evaluate((listItem, classSpecific ) => {
      const item = {
        name: null,
        slot: null,
        rarity: null,
        primary: [],
        secondary: [],
        class: null,
        special: null,
        specialPercent: null,
        magicProperties: [],
        setName: null,
        setProperties: [],
      };

      item.name = listItem.querySelector('h3')?.innerText;
      [item.rarity, ...item.slot] = listItem.querySelector('ul.item-type')?.innerText.split(' ');
      // todo: this should be obtained from the detail page (similarly to how set name is obtained for set items)
      item.slot = Array.isArray(item.slot) ? item.slot.join(' ') : item.slot; 
      const details = listItem.querySelector('ul.item-effects')?.children;
      let currentSection = 'secondary';
      details && Array.from(details).forEach((element) => {
        if (element.innerText === 'Primary') {
          currentSection = 'primary';
        } else if (element.innerText === 'Secondary') {
          currentSection = 'secondary';
        } else if (element.className === 'd3-color-ff6969ff') {
          item[currentSection].push(element.innerText);
        } else if (element.className === 'd3-color-ffff8000') {
          item.special = element.innerText;
        } else if (element.className === 'd3-color-ff9b9b9b') {
          item.specialPercent = element.innerText;
        } else if (classSpecific || element.className === 'd3-color-ffff0000') {
          item.class = titleCase(classSpecific) || element.innerText.replace(/\(| Only\)/g, '');
        } else if (element.className === 'item-effects-choice') {
          const effects = element.innerText.split('\n').map((txt) => txt.trim());
          item.magicProperties.push({ description: effects[0], values: effects.slice(1) });
        } else if (element.className === 'd3-color-blue') {
          item.magicProperties.push({ description: element.innerText, values: [] });
        }
      });

      const setDetails = listItem.querySelector('ul.item-itemset')?.children;
      setDetails && Array.from(setDetails).forEach((element) => {
        const effects = element.innerText && element.innerText.split('\n').map((txt) => txt.trim());
        if (effects) {
          item.setProperties.push({ description: effects[0], values: effects.slice(1) });
        }
      });

      return item;
    }, classSpecific);

    if (item.rarity === 'Set') {
      const escapedName = this.escapeXpathString(item.name);
      const nameLinks = await puppeteerElement.$x(`//a[contains(text(), ${escapedName})]`);
      const nameLink = nameLinks[0];
      const url = await nameLink.evaluate(elem => {
        return(elem.getAttribute('href'));
      });
      const newPage = await this.browser.newPage();
      await newPage.goto(`${this.urlRoot}${url}`);
      item.setName = await newPage.evaluate(() => {
        return document.querySelector('.detail-text .item-itemset-name').innerText;
      })
      await newPage.close();
    }

    return item;
  }

  async scrapeCurrentPage(classSpecific) {
    const pageItems = [];

    // due to how puppeteer handles context and functions within the evaluate statement,
    //    this needs to be set up once per page within thew window context.
    this.page.evaluate(() =>{
      window.titleCase = function titleCase(str) {
        return str && `${str[0].toUpperCase()}${str.substr(1).toLowerCase()}`;
      }
    })
    
    let currentItemsList = await this.page.$$('.db-table tr.row1, .db-table tr.row2');
    const len = currentItemsList.length;
    
    for(let i = 0; i < len; i++) {
      pageItems.push(await this.scrapeItem(currentItemsList[i], classSpecific));
    }

    return pageItems;
  }

  async scrapeSite() {
    const scrapedItems = [];

    // loop over all of the item slots (including class-specific slots)
    for (let slotIndex = 0; slotIndex < this.itemSlotCount; slotIndex++) {
      // for each slot, loop over the items slots that we are concerned with
      for (let typeIndex = 0; typeIndex < this.itemTypeCount; typeIndex++) {
        const itemSlotPage = this.itemSlotList[slotIndex];
        const itemType = this.itemTypeList[typeIndex];
        
        //todo: lump the following together with a tupple or something
        const hasSetItems = await this.page.evaluate(()  => {
          return !!document.querySelector('a[data-type="set"]')
        });
        const pageCount = await this.page.evaluate(()  => {
          const pagination = document.querySelector('.ui-pagination');
          return pagination ? pagination.children.length : 1;
        });
        const classSpecific = await this.page.evaluate(() => {
          const isSpecificFor = document.querySelector('.item-class-specific');
          return isSpecificFor ? isSpecificFor.innerText.split(' ')[0] : false;
        });
        // there will always be at least one page
        let currentPage = 1;
        
        // boo... magic strings
        if (itemType !== 'set' || itemType === 'set' && hasSetItems) {
          await this.page.goto('about:blank'); // since the site utilizes hashtag routing, this makes sure that every pass triggers a fresh load
          await this.page.goto(`${this.urlRoot}${itemSlotPage}${this.typeParam}${itemType}${this.pageParam}${currentPage}`);
          console.log(`scraping [${itemSlotPage}] [${itemType}] [page: ${currentPage}]`);
          scrapedItems.push(...await this.scrapeCurrentPage(classSpecific));

          // if more than one page, continue adding to the dataset
          while (currentPage < pageCount) {
            currentPage++;
            await this.page.goto('about:blank');
            await this.page.goto(`${this.urlRoot}${itemSlotPage}${this.typeParam}${itemType}${this.pageParam}${currentPage}`);
            console.log(`scraping [${itemSlotPage}] [${itemType}] [page: ${currentPage}]`);
            scrapedItems.push(...await this.scrapeCurrentPage());
          }
        }
      }
    }

    return scrapedItems;
  }
}

D3Scraper.defaultItemSlotList = [
  '/en/item/helm/',
  '/en/item/spirit-stone/',
  '/en/item/voodoo-mask/',
  '/en/item/wizard-hat/',
  '/en/item/pauldrons/',
  '/en/item/chest-armor/',
  '/en/item/cloak/',
  '/en/item/bracers/',
  '/en/item/gloves/',
  '/en/item/belt/',
  '/en/item/mighty-belt/',
  '/en/item/mighty-belt/',
  '/en/item/pants/',
  '/en/item/boots/',
  '/en/item/amulet/',
  '/en/item/ring/',
  '/en/item/shield/',
  '/en/item/crusader-shield/',
  '/en/item/mojo/',
  '/en/item/orb/',
  '/en/item/quiver/',
  '/en/item/phylactery/',
  // '/en/item/enchantress-focus/', // not really of interest
  // '/en/item/scoundrel-token/', // not really of interest
  // '/en/item/templar-relic/', // not really of interest
  '/en/item/axe-1h/',
  '/en/item/dagger/',
  '/en/item/mace-1h/',
  '/en/item/spear/',
  '/en/item/sword-1h/',
  '/en/item/ceremonial-knife/',
  '/en/item/fist-weapon/',
  '/en/item/flail-1h/',
  '/en/item/mighty-weapon-1h/',
  '/en/item/scythe-1h/',
  '/en/item/axe-2h/',
  '/en/item/mace-2h/',
  '/en/item/polearm/',
  '/en/item/staff/',
  '/en/item/sword-2h/',
  '/en/item/daibo/',
  '/en/item/flail-2h/',
  '/en/item/mighty-weapon-2h/',
  '/en/item/scythe-2h/',
  '/en/item/bow/',
  '/en/item/crossbow/',
  '/en/item/hand-crossbow/',
  '/en/item/wand/',
];

D3Scraper.defaultItemTypeList = [
  'legendary',
  'set',
];

async function main() {
  const startTime = Date.now();
  console.log('Starting');
  const isInTest = !!process.env.TEST;

  // puppeteer setup
  const browser = await puppeteer.launch({
    headless: isInTest ? false : true,
    devtools: isInTest ? true : false,
  });
  const page = await browser.newPage();

  const d3Scraper = new D3Scraper(page, browser);
  const items = await d3Scraper.scrapeSite();

  fs.writeFile('output.json', JSON.stringify(items, null, 2));

  // log items to browser console for easy inspection
  isInTest && await page.evaluate((_items) => {
    console.log(_items);
  }, items);

  // dont kill the browser if we are in test mode
  !isInTest && await browser.close();

  const duration = Date.now() - startTime;

  console.log(`Ending. Run took ${Math.floor((duration / (1000 * 60)) % 60)}:${Math.floor((duration / 1000) % 60)}`);
}

main();
